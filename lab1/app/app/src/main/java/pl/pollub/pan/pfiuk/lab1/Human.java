package pl.pollub.pan.pfiuk.lab1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor(suppressConstructorProperties = true)
public class Human {
    private String firstName;
    private String lastName;
    private int age;

    public String getWholeName() {
        return firstName + " " + lastName;
    }
}
