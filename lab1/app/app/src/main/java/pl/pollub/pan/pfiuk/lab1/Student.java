package pl.pollub.pan.pfiuk.lab1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import static java.util.Objects.isNull;

@Getter
@Setter
@NoArgsConstructor
public class Student extends Human {
    private String indexNumber;
    private final Map<String, List<Double>> subjectGrades = new HashMap<>();

    public Student(String firstName, String lastName, int age, String indexNumber) {
        super(firstName, lastName, age);
        this.indexNumber = indexNumber;
    }

    public Student withMark(String subject, double mark) {
        addMark(subject, mark);
        return this;
    }

    public void addMark(String subject, double mark) {
        if (isNull(subjectGrades.get(subject))) {
            subjectGrades.put(subject, new ArrayList<Double>());
        }

        subjectGrades.get(subject)
                     .add(mark);
    }

    public String getGradesAsString() {
        StringBuilder builder = new StringBuilder();

        for (String subject : subjectGrades.keySet()) {
            builder.append(subject)
                   .append('\t');
            for (Double grade : subjectGrades.get(subject)) {
                builder.append(grade)
                       .append("  ");
            }
            builder.append('\n');
        }

        return builder.toString();
    }

    public double calculateGradePointAverage() {
        double gradesSum = 0;
        int gradesAmount = 0;

        for (List<Double> gradesList : subjectGrades.values()) {
            for (Double grade : gradesList) {
                gradesSum += grade;
                ++gradesAmount;
            }
        }

        return (gradesSum / gradesAmount);
    }

    public List<Double> getGrades() {
        List<Double> result = new ArrayList<>();

        for (List<Double> gradesList : subjectGrades.values()) {
            result.addAll(gradesList);
        }

        return result;
    }

    public static double calculateGradePointAverage(List<Student> students) {
        double gradesSum = 0;
        int gradesAmount = 0;

        for (Student student : students) {
            List<Double> grades = student.getGrades();
            gradesAmount += grades.size();

            for (Double grade : grades) {
                gradesSum += grade;
            }
        }

        return gradesSum / gradesAmount;
    }
}
