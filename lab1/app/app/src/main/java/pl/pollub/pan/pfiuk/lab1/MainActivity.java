package pl.pollub.pan.pfiuk.lab1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.student_description)
    TextView studentDescription;

    List<Student> students = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        students.add(new Student("John", "Smith", 20, "AB1234").withMark("Java Language", 5)
                                                               .withMark("Java Language", 4)
                                                               .withMark("Prolog", 2));

        students.add(new Student("Ann", "Wood", 20, "AB1235").withMark("Java Language", 3)
                                                             .withMark("Prolog", 4));

        studentDescription.setText("");

        for (Student student : students) {
            studentDescription.append(student.getWholeName());
            studentDescription.append("\n");
            studentDescription.append(student.getGradesAsString());
            studentDescription.append("\n\nAverage: " + student.calculateGradePointAverage());
            studentDescription.append("\n----------------------------\n\n");
        }

        studentDescription.append("Group average: " + Student.calculateGradePointAverage(students));
    }
}
