package pl.pollub.pan.pfiuk.lab1;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class StudentFromInstruction extends Human {
    private static final int MARKS_AMOUNT = 20;

    private String indexNumber;
    private final String[][] subjectMarks = new String[MARKS_AMOUNT][2];
    private int counter = 0;

    public StudentFromInstruction(String firstName, String lastName, int age, String indexNumber) {
        super(firstName, lastName, age);
        this.indexNumber = indexNumber;
    }

    public void addMark(String subject, double mark) {
        if (counter == MARKS_AMOUNT) {
            throw new RuntimeException("Already added " + MARKS_AMOUNT + " marks.");
        }

        subjectMarks[counter][0] = subject;
        subjectMarks[counter][1] = String.valueOf(mark);

        ++counter;
    }

    public String printMarks() {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < counter; ++i) {
            builder.append(subjectMarks[i][0])
                   .append('\t')
                   .append(subjectMarks[i][1])
                   .append('\n');
        }

        return builder.toString();
    }
}
