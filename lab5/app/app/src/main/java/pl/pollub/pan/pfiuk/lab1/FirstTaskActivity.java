package pl.pollub.pan.pfiuk.lab1;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import java.util.Random;

import static android.graphics.Bitmap.Config.RGB_565;
import static android.graphics.Color.BLACK;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import static android.widget.RelativeLayout.CENTER_IN_PARENT;

public class FirstTaskActivity extends AppCompatActivity {

    private static final int RECTANGLE_WIDTH = 70;
    private static final int RECTANGLE_HEIGHT = 140;
    private static final int CIRCLE_RADIUS = 50;

    private int width;
    private int height;

    private Random random = new Random();

    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(prepareLayout());

        initializeDisplaySize();

        drawRandomRectangles(100);
        drawRandomCircles(20);
    }

    private RelativeLayout prepareLayout() {
        RelativeLayout layout = new RelativeLayout(this);
        layout.setBackgroundColor(BLACK);

        imageView = new ImageView(this);

        LayoutParams params = new LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
        params.addRule(CENTER_IN_PARENT);

        layout.addView(imageView);
        return layout;
    }

    private void initializeDisplaySize() {
        Point size = new Point();
        getWindowManager().getDefaultDisplay()
                          .getSize(size);
        width = size.x;
        height = size.y;
    }

    private void drawRandomRectangles(int amount) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, RGB_565);

        Canvas canvas = new Canvas(bitmap);

        Paint paint = new Paint();

        for (int i = 0; i < amount; ++i) {
            paint.setColor(getRandomColor());
            int positionX = random.nextInt(width);
            int positionY = random.nextInt(height);

            if (positionX + RECTANGLE_WIDTH > width) {
                positionX = width - RECTANGLE_WIDTH;
            }
            if (positionY + RECTANGLE_HEIGHT > height) {
                positionY = height - RECTANGLE_HEIGHT;
            }

            canvas.drawRect(positionX, positionY,
                            positionX + RECTANGLE_WIDTH,
                            positionY + RECTANGLE_HEIGHT,
                            paint);
        }

        imageView.setImageBitmap(bitmap);
    }

    private void drawRandomCircles(int amount) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, RGB_565);

        Canvas canvas = new Canvas(bitmap);

        Paint paint = new Paint();

        for (int i = 0; i < amount; ++i) {
            paint.setColor(getRandomColor());
            int positionX = random.nextInt(width);
            int positionY = random.nextInt(height);

            if (positionX + CIRCLE_RADIUS > width) {
                positionX = width - CIRCLE_RADIUS;
            }
            if (positionY + CIRCLE_RADIUS > height) {
                positionY = height - CIRCLE_RADIUS;
            }

            canvas.drawCircle(positionX, positionY, CIRCLE_RADIUS, paint);
        }

        imageView.setImageBitmap(bitmap);
    }

    private int getRandomColor() {
        return Color.rgb(random.nextInt(), random.nextInt(), random.nextInt());
    }
}
