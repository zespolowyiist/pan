package pl.pollub.pan.pfiuk.lab1.ball;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.MotionEvent;
import android.view.View;

import lombok.Getter;
import lombok.Setter;

import static android.graphics.Color.BLUE;
import static android.graphics.Color.RED;
import static android.view.MotionEvent.ACTION_MOVE;

public class BouncingView extends View {

    @Setter
    private float ballRadius;
    @Setter
    private float speedX;
    @Setter
    private float speedY;

    private float ballRadiusY;

    private float maxPositionX;
    private float maxPositionY;
    private float positionX;
    private float positionY;

    private float previousTouchX;
    private float previousTouchY;

    private Paint ballPaint = new Paint();
    private Paint lightPaint = new Paint();
    private RectF currentBallPosition = new RectF(0, 0, 0, 0);
    private RectF currentLightPosition = new RectF(0, 0, 0, 0);

    public BouncingView(Context context) {
        super(context);

        ballPaint.setColor(RED);
        lightPaint.setColor(Color.argb(100, 255, 255, 255));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawOval(currentBallPosition, ballPaint);
        canvas.drawOval(currentLightPosition, lightPaint);
        update();
        invalidate();
    }

    void update() {
        positionX += speedX;

        if (positionX < 0) {
            positionX = 0;
            speedX *= (-1);
        }
        if (positionX + ballRadius > maxPositionX) {
            positionX = maxPositionX - ballRadius;
            speedX *= (-1);
        }

        positionY += speedY;

        if (positionY < 0) {
            positionY = 0;
            speedY *= (-1);
        }
        if (positionY + (ballRadius / 2) > maxPositionY) {
            positionY = maxPositionY - (ballRadius / 2);
            speedY *= (-1);
        }

        refreshBallPosition();
    }

    private void refreshBallPosition() {
        if (positionY + ballRadius < maxPositionY)
            ballRadiusY = ballRadius;
        else
            ballRadiusY = maxPositionY - positionY;


        currentBallPosition.set(positionX,
                                positionY,
                                positionX + ballRadius,
                                positionY + ballRadiusY);

        currentLightPosition.set(positionX + (ballRadius / 4), positionY + (ballRadius / 4),
                                 positionX + ballRadius - (ballRadius / 2),
                                 positionY + ballRadiusY - (ballRadiusY / 2));
    }

    @Override
    protected void onSizeChanged(int width, int height, int oldWidth, int oldHeight) {
        maxPositionX = width - 1;
        maxPositionY = height - 1;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float currentX = event.getX();
        float currentY = event.getY();

        if (event.getAction() == ACTION_MOVE) {
            float deltaX = currentX - previousTouchX;
            float deltaY = currentY - previousTouchY;

            speedX = deltaX * ((maxPositionX / 2) / maxPositionX);
            speedY = deltaY * ((maxPositionY / 2) / maxPositionY);
        }

        previousTouchX = currentX;
        previousTouchY = currentY;

        return true;
    }
}
