package pl.pollub.pan.pfiuk.lab1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import pl.pollub.pan.pfiuk.lab1.ball.BouncingView;

import static android.graphics.Color.BLACK;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import static android.widget.RelativeLayout.CENTER_IN_PARENT;

public class MainActivity extends AppCompatActivity {

    private static final int BALL_RADIUS = 50;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(prepareLayout());
    }

    private RelativeLayout prepareLayout() {
        RelativeLayout layout = new RelativeLayout(this);
        layout.setBackgroundColor(BLACK);

        BouncingView bouncingView = new BouncingView(this);
        bouncingView.setBallRadius(BALL_RADIUS);
        bouncingView.setSpeedX(3);
        bouncingView.setSpeedY(2);

        LayoutParams params = new LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
        params.addRule(CENTER_IN_PARENT);

        layout.addView(bouncingView);
        return layout;
    }
}
