package pl.pollub.pan.pfiuk.lab1;

import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.io.File;

import butterknife.BindView;
import lombok.SneakyThrows;
import lombok.val;

import static android.media.MediaRecorder.AudioEncoder.DEFAULT;
import static android.media.MediaRecorder.AudioSource.MIC;
import static android.media.MediaRecorder.OutputFormat.THREE_GPP;
import static android.os.Environment.getExternalStorageDirectory;
import static butterknife.ButterKnife.bind;
import static java.io.File.separator;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.file_name_edit_text)
    EditText fileNameEditText;

    @BindView(R.id.toggle_recording_button)
    ToggleButton toggleRecordingButton;

    @BindView(R.id.play_recording_button)
    Button playRecordingButton;

    @BindView(R.id.go_to_camera_button)
    Button goToCameraButton;

    @BindView(R.id.console_text_view)
    TextView consoleTextView;

    private String outputFilePath;
    private MediaRecorder recorder = new MediaRecorder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bind(this);

        toggleRecordingButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                printDebug("Toggle recording button state changed to " + (isChecked ? "on" : "off"));

                if (isChecked) {
                    prepareOutputFilePath();
                    startAudioRecording();
                } else {
                    stopAudioRecording();
                }
            }
        });

        playRecordingButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                printDebug("Play button clicked");
                playAudioRecording();
            }
        });

        goToCameraButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, CameraActivity.class));
            }
        });
    }

    private void prepareOutputFilePath() {
        outputFilePath = getExternalStorageDirectory().getAbsolutePath()
                + separator
                + fileNameEditText.getText()
                                  .toString()
                + ".3gp";

        printDebug("Output file name: " + outputFilePath);
    }

    private void startAudioRecording() {
        prepareRecorder();
        recorder.start();
    }

    @SneakyThrows
    private MediaRecorder prepareRecorder() {
        recorder.setAudioSource(MIC);
        recorder.setOutputFormat(THREE_GPP);
        recorder.setOutputFile(outputFilePath);
        recorder.setAudioEncoder(DEFAULT);
        recorder.prepare();
        return recorder;
    }

    private void stopAudioRecording() {
        recorder.stop();
        recorder.release();
    }

    private void playAudioRecording() {
        val mediaPlayer = getMediaPlayer();
        printDebug("Starting to play audio");
        mediaPlayer.start();
    }

    @SneakyThrows
    private MediaPlayer getMediaPlayer() {
        val mediaPlayer = new MediaPlayer();
        mediaPlayer.setDataSource(outputFilePath);
        mediaPlayer.prepare();
        return mediaPlayer;
    }

    private void printDebug(String toPrint) {
        consoleTextView.append(toPrint + "\n");
    }
}
