package pl.pollub.pan.pfiuk.lab1;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

import butterknife.BindView;
import lombok.SneakyThrows;
import lombok.val;

import static android.net.Uri.fromFile;
import static android.os.Environment.DIRECTORY_PICTURES;
import static android.os.Environment.getExternalStoragePublicDirectory;
import static android.provider.MediaStore.ACTION_IMAGE_CAPTURE;
import static android.provider.MediaStore.EXTRA_OUTPUT;
import static android.provider.MediaStore.Images.Media.getBitmap;
import static butterknife.ButterKnife.bind;
import static java.io.File.separator;
import static java.util.UUID.randomUUID;

public class CameraActivity extends AppCompatActivity {

    @BindView(R.id.take_picture_button)
    Button takePictureButton;

    @BindView(R.id.photo_image_view)
    ImageView photoImageView;

    @BindView(R.id.photo_file_path_text_view)
    TextView photoFilePathTextView;

    private String photoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        bind(this);

        takePictureButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(ACTION_IMAGE_CAPTURE);
                photoPath = getPhotoPath();
                intent.putExtra(EXTRA_OUTPUT, getPhotoPathAsUri());
                startActivityForResult(intent, 1);
            }
        });
    }

    @Override
    @SneakyThrows
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK) {
            val bitmap = getBitmap(getContentResolver(), getPhotoPathAsUri());
            photoImageView.setImageBitmap(bitmap);
            photoFilePathTextView.setText(photoPath);
        }
    }

    private Uri getPhotoPathAsUri() {
        return fromFile(new File(photoPath));
    }

    private String getPhotoPath() {
        File storageDirectory = new File(getExternalStoragePublicDirectory(DIRECTORY_PICTURES), "cameraApp");

        return storageDirectory.getPath().replace("file", "content") + separator + randomUUID() + ".jpg";

    }


}
