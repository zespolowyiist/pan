package pl.pollub.pan.pfiuk.lab1;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.pollub.pan.pfiuk.lab1.database.entity.Event;

public class EditEventActivity extends AppCompatActivity {
    private static final String TAG = EditEventActivity.class.getName();

    @BindView(R.id.edit_select_date_button)
    Button selectDateButton;

    @BindView(R.id.edit_event_id)
    TextView eventIdView;

    @BindView(R.id.edit_form_name_field)
    EditText nameField;

    @BindView(R.id.edit_form_date_field)
    EditText dateField;

    @BindView(R.id.edit_form_place_field)
    EditText placeField;

    @BindView(R.id.edit_form_time_field)
    EditText timeField;

    @BindView(R.id.edit_record)
    Button editRecordButton;

    private Calendar calendar = Calendar.getInstance();
    private Event event;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "Event edition starting");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        ButterKnife.bind(this);

        Intent intent = getIntent();
        final Long eventId = intent.getLongExtra("editEventId", -1);
        if (eventId == -1) {
            throw new RuntimeException("Persistence error, database ID is invalid");
        }

        event = Event.findById(Event.class, eventId);
        eventIdView.setText("Event id = ");
        eventIdView.append(String.valueOf(eventId));
        nameField.setText(event.getName());
        placeField.setText(event.getPlace());
        dateField.setText(event.getDate());
        timeField.setText(event.getTime());

        final DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                updateDateField();
            }
        };

        selectDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(EditEventActivity.this, dateListener,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        editRecordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = nameField.getText()
                                       .toString();
                String place = placeField.getText()
                                         .toString();
                String date = dateField.getText()
                                       .toString();

                try {
                    new SimpleDateFormat("dd.MM.yy", Locale.getDefault()).parse(date);
                } catch (ParseException e) {
                    Toast.makeText(EditEventActivity.this, "Date should have DD.MM.YY format", Toast.LENGTH_SHORT)
                         .show();
                    return;
                }

                String time = timeField.getText()
                                       .toString();

                try {
                    new SimpleDateFormat("HH:mm", Locale.getDefault()).parse(time);
                } catch (ParseException e) {
                    Toast.makeText(EditEventActivity.this, "Time should have HH:MM format", Toast.LENGTH_SHORT)
                         .show();
                    return;
                }

                event.setName(name);
                event.setPlace(place);
                event.setDate(date);
                event.setTime(time);
                event.save();
                finish();
                Log.d(TAG, "Event edition done");
            }
        });
    }

    private void updateDateField() {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yy", Locale.getDefault());

        dateField.setText(format.format(calendar.getTime()));
    }
}
