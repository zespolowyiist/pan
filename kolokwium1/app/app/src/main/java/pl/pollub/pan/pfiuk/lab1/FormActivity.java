package pl.pollub.pan.pfiuk.lab1;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.text.ParseException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.pollub.pan.pfiuk.lab1.database.entity.Event;
import pl.pollub.pan.pfiuk.lab1.utils.EventListAdapter;

public class FormActivity extends AppCompatActivity {

    @BindView(R.id.events_list)
    ListView eventsList;

    @BindView(R.id.select_date_button)
    Button selectDateButton;

    @BindView(R.id.form_name_field)
    EditText nameField;

    @BindView(R.id.form_date_field)
    EditText dateField;

    @BindView(R.id.form_place_field)
    EditText placeField;

    @BindView(R.id.form_time_field)
    EditText timeField;

    @BindView(R.id.add_new_record)
    Button addNewRecordButton;

    private List<Event> eventList = Event.listAll(Event.class);
    private EventListAdapter temperatureListAdapter;

    private Calendar calendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        ButterKnife.bind(this);

        final DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                updateDateField();
            }
        };

        selectDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(FormActivity.this, dateListener,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        temperatureListAdapter = new EventListAdapter(this, R.layout.events_list_item,
                eventList);
        eventsList.setAdapter(temperatureListAdapter);
        temperatureListAdapter.notifyDataSetChanged();

        eventsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Event event = eventList.get(position);
                Intent intent = new Intent(getApplicationContext(), EditEventActivity.class);
                intent.putExtra("editEventId", event.getId());
                startActivity(intent);
            }
        });

        addNewRecordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = nameField.getText()
                                       .toString();
                String place = placeField.getText()
                                         .toString();
                String date = dateField.getText()
                                       .toString();

                try {
                    new SimpleDateFormat("dd.MM.yy", Locale.getDefault()).parse(date);
                } catch (ParseException e) {
                    Toast.makeText(FormActivity.this, "Date should have DD.MM.YY format", Toast.LENGTH_SHORT)
                         .show();
                    return;
                }

                String time = timeField.getText()
                                       .toString();

                try {
                    new SimpleDateFormat("HH:mm", Locale.getDefault()).parse(time);
                } catch (ParseException e) {
                    Toast.makeText(FormActivity.this, "Time should have HH:MM format", Toast.LENGTH_SHORT)
                         .show();
                    return;
                }

                Event event = new Event(name, place, date, time);

                event.save();

                eventList.add(event);
                temperatureListAdapter.notifyDataSetChanged();
                Log.d("FormActivity", "Data set changed, item count: " + eventList.size());
            }
        });
    }

    private void updateDateField() {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yy", Locale.getDefault());

        dateField.setText(format.format(calendar.getTime()));
    }

    @Override
    protected void onResume() {
        super.onResume();
        temperatureListAdapter.refreshEvents();
    }
}
