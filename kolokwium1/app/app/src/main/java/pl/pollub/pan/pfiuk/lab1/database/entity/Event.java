package pl.pollub.pan.pfiuk.lab1.database.entity;

import com.orm.SugarRecord;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor(suppressConstructorProperties = true)
public class Event extends SugarRecord {
    private String name;
    private String place;
    private String date;
    private String time;
}
