package pl.pollub.pan.pfiuk.lab1.utils;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import pl.pollub.pan.pfiuk.lab1.R;
import pl.pollub.pan.pfiuk.lab1.database.entity.Event;

public class EventListAdapter extends ArrayAdapter<Event> {
    private static final String TAG = EventListAdapter.class.getName();
    List<Event> items;

    public EventListAdapter(Context context, int resource, List<Event> items) {
        super(context, resource, items);
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;

        if (view == null) {
            LayoutInflater inflater;
            inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.events_list_item, null);
        }

        Event event = getItem(position);

        if (event != null) {
            TextView nameField = (TextView) view.findViewById(R.id.event_item_name);
            nameField.setText(event.getName());

            TextView placeField = (TextView) view.findViewById(R.id.event_item_place);
            placeField.setText(event.getPlace());

            TextView dateField = (TextView) view.findViewById(R.id.event_item_date);
            dateField.setText(event.getDate());

            TextView timeField = (TextView) view.findViewById(R.id.event_item_time);
            timeField.setText(String.valueOf(event.getTime()));
        }

        return view;
    }

    public void refreshEvents() {
        for (int i = 0; i < items.size(); ++i)
            refreshEvent(i);

        notifyDataSetChanged();
    }

    public void refreshEvent(int position) {
        Event event = getItem(position);
        Log.d(TAG, "Refreshing event: " + event.toString());
        Event fromDatabase = Event.findById(Event.class, event.getId());
        event.setName(fromDatabase.getName());
        event.setPlace(fromDatabase.getPlace());
        event.setDate(fromDatabase.getDate());
        event.setTime(fromDatabase.getTime());
        Log.d(TAG, "Refreshed: " + event.toString());
    }
}
