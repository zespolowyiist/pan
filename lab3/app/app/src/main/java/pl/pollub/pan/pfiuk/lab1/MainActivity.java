package pl.pollub.pan.pfiuk.lab1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.pollub.pan.pfiuk.lab1.credentials.CredentialsRepository;
import pl.pollub.pan.pfiuk.lab1.credentials.TestCredentialsRepository;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.login_edit_field)
    EditText loginEditField;

    @BindView(R.id.password_edit_field)
    EditText passwordEditField;

    @BindView(R.id.login_submit_button)
    Button submitButton;

    private CredentialsRepository credentialsRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        credentialsRepository = new TestCredentialsRepository(getApplicationContext());

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String login = loginEditField.getText()
                                             .toString();
                String password = passwordEditField.getText()
                                                   .toString();

                if (!credentialsRepository.isValid(login, password))
                    return;

                Intent intent = new Intent(getApplicationContext(), FormActivity.class);
                startActivity(intent);
            }
        });
    }
}
