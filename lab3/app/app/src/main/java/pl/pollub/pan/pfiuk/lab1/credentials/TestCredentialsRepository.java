package pl.pollub.pan.pfiuk.lab1.credentials;

import android.content.Context;
import android.widget.Toast;

import pl.pollub.pan.pfiuk.lab1.database.entity.User;

public class TestCredentialsRepository implements CredentialsRepository {
    private static final String TEST_VALID_LOGIN = "secret";
    private static final String TEST_VALID_PASSWORD = "passwd";

    private Context context;

    public TestCredentialsRepository(Context context) {
        this.context = context;
    }

    @Override
    public boolean isValid(User user) {
        return isValid(user.getLogin(), user.getPassword());
    }

    @Override
    public boolean isValid(String login, String password) {
        if (login.isEmpty())
            return true;

        if (!login.equals(TEST_VALID_LOGIN)) {
            Toast.makeText(context, "Invalid login", Toast.LENGTH_SHORT)
                 .show();
            return false;
        }
        if (!password.equals(TEST_VALID_PASSWORD)) {
            Toast.makeText(context, "Invalid password", Toast.LENGTH_SHORT)
                 .show();
            return false;
        }

        return true;
    }
}
