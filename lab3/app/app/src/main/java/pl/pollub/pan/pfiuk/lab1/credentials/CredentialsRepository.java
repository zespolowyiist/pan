package pl.pollub.pan.pfiuk.lab1.credentials;

import pl.pollub.pan.pfiuk.lab1.database.entity.User;

public interface CredentialsRepository {
    boolean isValid(User user);
    boolean isValid(String user, String password);
}
