package pl.pollub.pan.pfiuk.lab1.database.entity;

import com.orm.SugarRecord;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(suppressConstructorProperties = true)
public class User extends SugarRecord {
    private String login;
    private String password;
}
