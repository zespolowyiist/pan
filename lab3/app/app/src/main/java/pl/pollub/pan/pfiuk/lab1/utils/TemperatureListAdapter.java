package pl.pollub.pan.pfiuk.lab1.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import pl.pollub.pan.pfiuk.lab1.R;
import pl.pollub.pan.pfiuk.lab1.database.entity.Temperature;

public class TemperatureListAdapter extends ArrayAdapter<Temperature> {
    private List<Temperature> temperatures;

    public TemperatureListAdapter(Context context, int resource, List<Temperature> items) {
        super(context, resource, items);
    }

//    @Override
//    public Temperature getItem(int position) {
//        temperatures.get(position);
//    }
//
//    @Override
//    public void add(Temperature temperature) {
//        temperatures.add(temperature);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;

        if (view == null) {
            LayoutInflater inflater;
            inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.temperatures_list_item, null);
        }

        Temperature temperature = getItem(position);

        if (temperature != null) {
            // TODO: przerobić na butterknife
            TextView placeField = (TextView) view.findViewById(R.id.temperature_item_place);
            placeField.setText(temperature.getPlace());

            TextView dateField = (TextView) view.findViewById(R.id.temperature_item_date);
            dateField.setText(temperature.getDate());

            TextView valueField = (TextView) view.findViewById(R.id.temperature_item_value);
            valueField.setText(String.valueOf(temperature.getValue()));
        }

        return view;
    }
}
