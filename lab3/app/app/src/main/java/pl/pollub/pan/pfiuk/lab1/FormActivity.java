package pl.pollub.pan.pfiuk.lab1;

import android.app.DatePickerDialog;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.pollub.pan.pfiuk.lab1.database.entity.Temperature;
import pl.pollub.pan.pfiuk.lab1.utils.TemperatureListAdapter;

public class FormActivity extends AppCompatActivity {

    @BindView(R.id.temperatures_list)
    ListView temperaturesList;

    @BindView(R.id.select_date_button)
    Button selectDateButton;

    @BindView(R.id.form_date_field)
    EditText dateField;

    @BindView(R.id.form_place_field)
    EditText placeField;

    @BindView(R.id.form_temperature_field)
    EditText temperatureField;

    @BindView(R.id.add_new_record)
    Button addNewRecordButton;

    private List<Temperature> temperatureList = Temperature.listAll(Temperature.class);
    private ArrayAdapter<?> temperatureListAdapter;

    private Calendar calendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        ButterKnife.bind(this);

        Toast.makeText(FormActivity.this, this.getClass()
                                              .getName() + ": on Create()", Toast.LENGTH_SHORT)
             .show();

        final DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                updateDateField();
            }
        };

        selectDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(FormActivity.this, dateListener,
                                     calendar.get(Calendar.YEAR),
                                     calendar.get(Calendar.MONTH),
                                     calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        temperatureListAdapter = new TemperatureListAdapter(this, R.layout.temperatures_list_item,
                                                            temperatureList);
        temperaturesList.setAdapter(temperatureListAdapter);
        temperatureListAdapter.notifyDataSetChanged();

        addNewRecordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String date = dateField.getText()
                                       .toString();
                String place = placeField.getText()
                                         .toString();
                Double value;
                try {
                    value = Double.valueOf(temperatureField.getText()
                                                           .toString());
                } catch (NumberFormatException e) {
                    Toast.makeText(FormActivity.this, "Temperature must not be empty.",
                                   Toast.LENGTH_SHORT)
                         .show();
                    return;
                }

                Temperature temperature = new Temperature(place, date, value);

                temperature.save();

                temperatureList.add(temperature);
                temperatureListAdapter.notifyDataSetChanged();
                Log.d("FormActivity", "Data set changed, item count: " + temperatureList.size());
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(FormActivity.this, this.getClass()
                                              .getName() + ": on Start()", Toast.LENGTH_SHORT)
             .show();
    }

    private void updateDateField() {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yy", Locale.getDefault());

        dateField.setText(format.format(calendar.getTime()));
    }
}
