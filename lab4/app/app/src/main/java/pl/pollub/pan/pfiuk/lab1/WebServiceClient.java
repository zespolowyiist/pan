package pl.pollub.pan.pfiuk.lab1;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

class WebServiceClient {
    private static final String BASE_URL = "http://services.aonaware.com/DictService/DictService.asmx/Define?word=";

    String getWordDefinition(String word) {
        try {
            return getWordDefinitionThrowable(word);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String getWordDefinitionThrowable(String word) throws Exception {
        HttpURLConnection con = openHttpConnection(BASE_URL + word);
        int response = con.getResponseCode();
        InputStream in;
        if (response == HttpURLConnection.HTTP_OK) {
            in = con.getInputStream();
        }
        else {
            throw new RuntimeException("HTTP connection status is " + response);
        }
        Document doc;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db;
        db = dbf.newDocumentBuilder();
        doc = db.parse(in);
        doc.getDocumentElement()
           .normalize();

        StringBuilder stringBuilder = new StringBuilder();
        NodeList definitionElements = doc.getElementsByTagName("Definition");
        for (int i = 0; i < definitionElements.getLength(); i++) {
            Node itemNode = definitionElements.item(i);
            if (itemNode.getNodeType() == Node.ELEMENT_NODE) {
                Element definitionElement = (Element) itemNode;

                NodeList wordDefinitionElements;
                wordDefinitionElements = definitionElement.getElementsByTagName("WordDefinition");

                for (int j = 0; j < wordDefinitionElements.getLength(); j++) {
                    Element wordDefinitionElement =
                            (Element) wordDefinitionElements.item(j);

                    NodeList textNodes = wordDefinitionElement.getChildNodes();
                    stringBuilder.append(textNodes.item(0)
                                                  .getNodeValue())
                                 .append(". \n");
                }
            }
        }
        return stringBuilder.toString();
    }

    private HttpURLConnection openHttpConnection(String urlString) {
        URLConnection connection;

        try {
            connection = new URL(urlString).openConnection();
        } catch (Exception e) {
            throw new RuntimeException("Error opening connection", e);
        }

        if (!(connection instanceof HttpURLConnection))
            throw new RuntimeException("Not an HTTP connection");
        try {
            HttpURLConnection httpConnection;
            httpConnection = (HttpURLConnection) connection;
            httpConnection.setAllowUserInteraction(false);
            httpConnection.setInstanceFollowRedirects(true);
            httpConnection.setRequestMethod("GET");
            httpConnection.connect();
            return httpConnection;
        } catch (Exception e) {
            throw new RuntimeException("Error connecting", e);
        }
    }
}
