package pl.pollub.pan.pfiuk.lab1;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

abstract class BaseImageTask extends AsyncTask<Void, Void, Bitmap> {
    private static final String TAG = BaseImageTask.class.getName();

    String imageUrl;
    private ImageView view;

    BaseImageTask(String imageUrl, ImageView view) {
        this.imageUrl = imageUrl;
        this.view = view;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        if (result != null) {
            Log.d(TAG, "Successfully loaded image " + imageUrl);

            view.setImageBitmap(result);
        }
        else {
            Log.e(TAG, "Adding image " + imageUrl
                    + " to view failed, setting up fallback warning image");

            view.setImageDrawable(view.getContext()
                                      .getDrawable(R.drawable.warning));
        }
    }
}
