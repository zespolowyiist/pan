package pl.pollub.pan.pfiuk.lab1;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.ImageView;

import java.io.InputStream;

class DownloadImageTask extends BaseImageTask {
    private static final String TAG = DownloadImageTask.class.getName();

    public DownloadImageTask(String imageUrl, ImageView view) {
        super(imageUrl, view);
    }

    protected Bitmap doInBackground(Void... args) {
        Log.d(TAG, "Loading image from " + imageUrl);

        Bitmap result;

        try {
            InputStream in = new java.net.URL(imageUrl).openStream();
            result = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e(TAG, "Downloading image from " + imageUrl + " failed", e);
            return null;
        }

        return result;
    }
}