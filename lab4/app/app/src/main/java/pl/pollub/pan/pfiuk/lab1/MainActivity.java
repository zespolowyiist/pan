package pl.pollub.pan.pfiuk.lab1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    private static final String imageUrl = "http://cs.pollub.pl/wpcontent/themes/instytut/images/logo_en.jpg";

    private static final String TAG = MainActivity.class.getName();

    private static final int BUFFER_SIZE = 128;

    @BindView(R.id.img)
    ImageView image;

    @BindView(R.id.word_to_search)
    EditText wordToSearch;

    @BindView(R.id.search_button)
    Button searchButton;

    @BindView(R.id.text)
    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        new DownloadImageTask(imageUrl, image).execute();

        Log.d(TAG, downloadText("http://wklej.org/id/2938528/txt/"));

        try {
            text.setText(new AccessWebServiceTask(new WebServiceClient()).execute("java")
                                                                         .get());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String downloadText(String url) {
        try {
            return downloadTextThrowable(url);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String downloadTextThrowable(String url) throws IOException {
        HttpURLConnection connection = openHttpConnection(url);
        connection.connect();

        int response = connection.getResponseCode();

        InputStreamReader inputStreamReader;

        if (response == HttpURLConnection.HTTP_OK) {
            inputStreamReader = new InputStreamReader(connection.getInputStream());
        }
        else {
            return "ERROR";
        }

        char[] inputBuffer = new char[BUFFER_SIZE];
        int readSize;
        StringBuilder result = new StringBuilder();
        while ((readSize = inputStreamReader.read(inputBuffer)) > 0) {
            String readString = String.copyValueOf(inputBuffer, 0, readSize);
            result.append(readString);
            inputBuffer = new char[BUFFER_SIZE];
        }

        inputStreamReader.close();
        connection.disconnect();

        return result.toString();
    }

    HttpURLConnection openHttpConnection(String urlString) {
        try {
            return (HttpURLConnection) new URL(urlString).openConnection();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
