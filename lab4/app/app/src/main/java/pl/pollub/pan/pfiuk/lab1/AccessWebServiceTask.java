package pl.pollub.pan.pfiuk.lab1;

import android.os.AsyncTask;

class AccessWebServiceTask extends AsyncTask<String, Void, String> {
    private WebServiceClient client;

    AccessWebServiceTask(WebServiceClient client) {
        this.client = client;
    }

    @Override
    protected String doInBackground(String... strings) {
        return client.getWordDefinition(strings[0]);
    }
}
