package pl.pollub.pan.pfiuk.lab1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import butterknife.ButterKnife;

public class FormActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        ButterKnife.bind(this);

        Toast.makeText(FormActivity.this, this.getClass()
                                              .getName() + ": on Create()", Toast.LENGTH_SHORT)
             .show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(FormActivity.this, this.getClass()
                                              .getName() + ": on Start()", Toast.LENGTH_SHORT)
             .show();
    }
}
