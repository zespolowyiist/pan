package pl.pollub.pan.pfiuk.lab1.credentials;

public interface CredentialsRepository {
    public boolean isValid(String user, String password);
}
